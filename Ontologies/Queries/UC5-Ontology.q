PREFIX uc5: <http://deepcube-h2020.eu/tourism/ontology#>

SELECT ?ndvi ?rastercell 

WHERE {
?rastercell a uc5:RasterCell .
?ndvi a uc5:NDVI .
?ndvi uc5:refersToRC ?rastercell .
}
LIMIT 1



PREFIX uc5: <http://deepcube-h2020.eu/tourism/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?rastercell ?value 

WHERE {
?rastercell a uc5:RasterCell ;
geo:asWKT ?wkt1 .
?ndvi a uc5:NDVI .
?ndvi uc5:refersToRC ?rastercell ;
       uc5:hasNDVI ?value .
<http://deepcube-h2020.eu/tourism/ontology#AdministrativeUnit/Maranhão>  a uc5:AdministrativeUnit ;
geo:asWKT ?wkt2 .
FILTER(geof:sfWithin(?wkt1,?wkt2)).
}
LIMIT 10
