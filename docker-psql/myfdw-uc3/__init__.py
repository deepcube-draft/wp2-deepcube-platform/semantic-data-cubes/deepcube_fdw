import numpy
from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres
import logging
import xarray as xr
from datetime import datetime, timedelta
import fsspec

#self.zarr_file -> uc3/uc3cube.zarr

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.zarr_file = fdw_options.get('zarr_file', None)
		if self.zarr_file is None:
			raise ValueError('The zarr_file option is mandatory')

	def execute(self, quals, columns):
		ds = xr.open_dataset(self.zarr_file)

		#y and x values for Greece
		#y_min = 19.33
		y_min = 34.93
		y_max = 41.62
		x_min = 19.86
		x_max = 28.19

		# 2009-03-06 ... 2020-12-26
		start_date = datetime.strptime("2009-03-06", "%Y-%m-%d")
		end_date = datetime.strptime("2020-12-26", "%Y-%m-%d")
		log_to_postgres('[SEMCUBE] Start looking for filters', logging.DEBUG)
		#check for filter condition here in order to avoid unnecessary access to zarr
		for qual in quals:
			if qual.field_name == 'x':
				if qual.operator == '>':
					x_min = float(qual.value)
				if qual.operator == '<':
					x_max = float(qual.value)
			if qual.field_name == 'y':
				if qual.operator == '>':
					y_min = float(qual.value)
				if qual.operator == '<':
					y_max = float(qual.value)
			if qual.field_name == 'time':
				if qual.operator == '>':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=1)
				if qual.operator == '<':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=-1)
				if qual.operator == '=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
					end_date = start_date + timedelta(minutes=1439)
		log_to_postgres('[SEMCUBE] Finished looking for filters', logging.DEBUG)
		log_to_postgres('[SEMCUBE] Slicing x_min: ' + str(x_min) + ' x_max:' + str(x_max) + ' y_min:' + str(y_min) + ' y_max:' + str(y_max) + ' start_date:' + str(start_date) + ' end_date:' + str(end_date)  , logging.DEBUG)
		sliced_ds = ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min),time=slice(start_date, end_date))
		log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)
		

		for timeindex in range(sliced_ds.time.size):
			log_to_postgres('[SEMCUBE] Sel timeindex: ' + str(timeindex) + ' out of:' + str(sliced_ds.time.size), logging.DEBUG)
			ds_subset = sliced_ds.isel(time=[timeindex]) #chunking day by day
			log_to_postgres('[SEMCUBE] Finished sel', logging.DEBUG)
			i = 0 #time index is always 0
			time = numpy.datetime_as_string(ds_subset.time.values[i], unit='s')
			#return time in this form, otherwise postgres checks f an equality constraint on time is valid and does not return any results
			
			
			log_to_postgres('[SEMCUBE] Finished column assignments', logging.DEBUG)
			

			
			log_to_postgres('[SEMCUBE] Start yieldng', logging.DEBUG)
			for j in range(ds_subset.y.size):
				for k in range(ds_subset.x.size):
					log_to_postgres('[SEMCUBE] loop for i: ' + str(i) + ' k:' + str(k) + ' j:' + str(j), logging.DEBUG)
					line = {}
					for column_name in columns:
						if (column_name == "time"):
							line[column_name] = time
						elif (column_name == "x"):
							line[column_name] = ds_subset.x.values[k]
						elif (column_name == "y"):
							line[column_name] = ds_subset.y.values[j]
						elif (column_name == "evi"):
							line[column_name] = ds_subset["1 km 16 days EVI"].values[i][j][k]
						elif (column_name == "ndvi"):
							line[column_name] = ds_subset["1 km 16 days NDVI"].values[i][j][k]
						elif (column_name == "quality"):
							line[column_name] = ds_subset["1 km 16 days VI Quality"].values[i][j][k]
						elif (column_name == "et_500m"):
							line[column_name] = ds_subset.ET_500m.values[i][j][k]
						elif (column_name == "et_qc_500m"):
							line[column_name] = ds_subset.ET_QC_500m.values[i][j][k]
						elif (column_name == "fparextra_qc"):
							line[column_name] = ds_subset.FparExtra_QC.values[i][j][k]
						elif (column_name == "fparlai_qc"):
							line[column_name] = ds_subset.FparLai_QC.values[i][j][k]
						elif (column_name == "fparstddev_500m"):
							line[column_name] = ds_subset.FparStdDev_500m.values[i][j][k]
						elif (column_name == "fpar_500m"):
							line[column_name] = ds_subset.Fpar_500m.values[i][j][k]
						elif (column_name == "le_500m"):
							line[column_name] = ds_subset.LE_500m.values[i][j][k]
						elif (column_name == "lst_day_1km"):
							line[column_name] = ds_subset.LST_Day_1km.values[i][j][k]
						elif (column_name == "lst_night_1km"):
							line[column_name] = ds_subset.LST_Night_1km.values[i][j][k]
						elif (column_name == "laistddev_500m"):
							line[column_name] = ds_subset.LaiStdDev_500m.values[i][j][k]
						elif (column_name == "lai_500m"):
							line[column_name] = ds_subset.Lai_500m.values[i][j][k]
						elif (column_name == "pet_500m"):
							line[column_name] = ds_subset.PET_500m.values[i][j][k]
						elif (column_name == "ple_500m"):
							line[column_name] = ds_subset.PLE_500m.values[i][j][k]
						elif (column_name == "qc_day"):
							line[column_name] = ds_subset.QC_Day.values[i][j][k]
						elif (column_name == "qc_night"):
							line[column_name] = ds_subset.QC_Night.values[i][j][k]
						elif (column_name == "aspect_max"):
							line[column_name] = ds_subset.aspect_max.values[j][k]
						elif (column_name == "aspect_mean"):
							line[column_name] = ds_subset.aspect_mean.values[j][k]
						elif (column_name == "aspect_min"):
							line[column_name] = ds_subset.aspect_min.values[j][k]
						elif (column_name == "aspect_std"):
							line[column_name] = ds_subset.aspect_std.values[j][k]
						elif (column_name == "burned_areas"):
							line[column_name] = ds_subset.burned_areas.values[i][j][k]
						elif (column_name == "clc_2006"):
							line[column_name] = ds_subset.clc_2006.values[j][k]
						elif (column_name == "clc_2012"):
							line[column_name] = ds_subset.clc_2012.values[j][k]
						elif (column_name == "clc_2018"):
							line[column_name] = ds_subset.clc_2018.values[j][k]
						elif (column_name == "dem_max"):
							line[column_name] = ds_subset.dem_max.values[j][k]
						elif (column_name == "dem_mean"):
							line[column_name] = ds_subset.dem_mean.values[j][k]
						elif (column_name == "dem_min"):
							line[column_name] = ds_subset.dem_min.values[j][k]
						elif (column_name == "dem_std"):
							line[column_name] = ds_subset.dem_std.values[j][k]
						elif (column_name == "era5_max_t2m"):
							line[column_name] = ds_subset.era5_max_t2m.values[i][j][k]
						elif (column_name == "era5_max_tp"):
							line[column_name] = ds_subset.era5_max_tp.values[i][j][k]
						elif (column_name == "era5_max_u10"):
							line[column_name] = ds_subset.era5_max_u10.values[i][j][k]
						elif (column_name == "era5_max_v10"):
							line[column_name] = ds_subset.era5_max_v10.values[i][j][k]
						elif (column_name == "era5_min_t2m"):
							line[column_name] = ds_subset.era5_min_t2m.values[i][j][k]
						elif (column_name == "era5_min_tp"):
							line[column_name] = ds_subset.era5_min_tp.values[i][j][k]
						elif (column_name == "era5_min_u10"):
							line[column_name] = ds_subset.era5_min_u10.values[i][j][k]
						elif (column_name == "era5_min_v10"):
							line[column_name] = ds_subset.era5_min_v10.values[i][j][k]
						elif (column_name == "fwi"):
							line[column_name] = ds_subset.fwi.values[i][j][k]
						elif (column_name == "ignition_points"):
							line[column_name] = ds_subset.ignition_points.values[i][j][k]
						elif (column_name == "number_of_fires"):
							line[column_name] = ds_subset.number_of_fires.values[i]
						elif (column_name == "population_density_2009"):
							line[column_name] = ds_subset.population_density_2009.values[j][k]
						elif (column_name == "population_density_2010"):
							line[column_name] = ds_subset.population_density_2010.values[j][k]
						elif (column_name == "population_density_2011"):
							line[column_name] = ds_subset.population_density_2011.values[j][k]
						elif (column_name == "population_density_2012"):
							line[column_name] = ds_subset.population_density_2012.values[j][k]
						elif (column_name == "population_density_2013"):
							line[column_name] = ds_subset.population_density_2013.values[j][k]
						elif (column_name == "population_density_2014"):
							line[column_name] = ds_subset.population_density_2014.values[j][k]
						elif (column_name == "population_density_2015"):
							line[column_name] = ds_subset.population_density_2015.values[j][k]
						elif (column_name == "population_density_2016"):
							line[column_name] = ds_subset.population_density_2016.values[j][k]
						elif (column_name == "population_density_2017"):
							line[column_name] = ds_subset.population_density_2017.values[j][k]
						elif (column_name == "population_density_2018"):
							line[column_name] = ds_subset.population_density_2018.values[j][k]
						elif (column_name == "population_density_2019"):
							line[column_name] = ds_subset.population_density_2019.values[j][k]
						elif (column_name == "population_density_2020"):
							line[column_name] = ds_subset.population_density_2020.values[j][k]
						elif (column_name == "roads_density_2020"):
							line[column_name] = ds_subset.roads_density_2020.values[j][k]
						elif (column_name == "slope_max"):
							line[column_name] = ds_subset.slope_max.values[j][k]
						elif (column_name == "slope_mean"):
							line[column_name] = ds_subset.slope_mean.values[j][k]
						elif (column_name == "slope_min"):
							line[column_name] = ds_subset.slope_min.values[j][k]
						elif (column_name == "slope_std"):
							line[column_name] = ds_subset.slope_std.values[j][k]
						else:
							raise ValueError('Unknown Column: ' + column_name)
					yield line
					log_to_postgres('[SEMCUBE] Yielded line', logging.DEBUG)



