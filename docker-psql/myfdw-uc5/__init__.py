from multicorn import ForeignDataWrapper
import xarray as xr
import fsspec

#self.zarr_file -> uc5/datacube_uc5.zarr

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.zarr_file = fdw_options.get('zarr_file', None)
		if self.zarr_file is None:
			raise ValueError('The zarr_file option is mandatory')

	def execute(self, quals, columns):
		#url = 'https://storage.de.cloud.ovh.net/v1/AUTH_84d6da8e37fe4bb5aea18902da8c1170' + self.zarr_file
		#ds = xr.open_zarr(fsspec.get_mapper(url), consolidated=True)
		ds = xr.open_zarr(self.zarr_file)
		timeVal = ds.time.values
		latVal = ds.lat.values
		lonVal = ds.lon.values
		asterVal = ds.ASTER_GDEM_DEM.values
		ch4Val = ds.CAMS_ch4.values
		coVal = ds.CAMS_co.values
		co3Val = ds.CAMS_co3.values
		noVal = ds.CAMS_no.values
		no2Val = ds.CAMS_no2.values
		so2Val = ds.CAMS_so2.values
		lc100Val = ds.LC100.values
		ndviVal = ds.NDVI.values
		ndviuVal = ds.NDVI_unc.values
		swi001Val = ds.SWI_001.values
		swi005Val = ds.SWI_005.values
		swi010Val = ds.SWI_010.values
		swi015Val = ds.SWI_015.values
		swi020Val = ds.SWI_020.values
		swi040Val = ds.SWI_040.values
		swi060Val = ds.SWI_060.values
		swi100Val = ds.SWI_100.values
		mrtVal = ds.THERMAL_MRT.values
		utciVal = ds.THERMAL_UTCI.values

		#check for filter condition
		qualsFlag = False
		qualsDict = {}

		latStart = 0
		latEnd = len(latVal)-1
		lonStart = 0
		lonEnd = len(lonVal)-1

		#TODO; Handle equality with math.isclose()
		for qual in quals:
			if qual.field_name == 'lat':
				if qual.operator == '>':
					min_lat = float(qual.value)
					for i in range(latStart, latEnd):
						if latVal[i] < min_lat:
							latEnd = i
							break
				if qual.operator == '<':
					max_lat = float(qual.value)
					for i in range(latEnd, latStart, -1):
						if latVal[i] > max_lat:
							latStart = i-1
							break
			if qual.field_name == 'lon':
				if qual.operator == '<':
					min_lon = float(qual.value)
					for i in range(lonStart, lonEnd):
						if lonVal[i] > min_lon:
							lonEnd = i
							break
				if qual.operator == '>':
					max_lon = float(qual.value)
					for i in range(lonEnd, lonStart, -1):
						if lonVal[i] < max_lon:
							lonStart = i-1
							break

		for i in range(0, len(timeVal)):
			for j in range(latStart, latEnd):
				for k in range(lonStart, lonEnd):
					line = [timeVal[i], latVal[j], lonVal[k], asterVal[j][k], ch4Val[i][j][k],
					coVal[i][j][k], co3Val[i][j][k], noVal[i][j][k], no2Val[i][j][k],
					so2Val[i][j][k], lc100Val[j][k], ndviVal[i][j][k], ndviuVal[i][j][k],
					swi001Val[i][j][k], swi005Val[i][j][k], swi010Val[i][j][k], swi015Val[i][j][k],
					swi020Val[i][j][k], swi040Val[i][j][k], swi060Val[i][j][k], swi100Val[i][j][k],
					mrtVal[i][j][k], utciVal[i][j][k]]
					yield line
